#include<stdio.h>
#include<math.h>

int factoriel(int n) {

	int result = 1;
	for (int i = n; i > 0; i--)
	{
		result *= i;
	}
	return result;
}
float sum(float x, int n) {
	float result = 0;
	int isNegative = 0;
	for (int i = 1; i >= n; i += 2)
	{
		if (isNegative)
		{
			result += (-1)*pow(x, i) / factoriel(i);
			isNegative = 1;
		}
		else {
			result += pow(x, i) / factoriel(i);
			isNegative = 0;
		}
		return result;

	}
}
int main() {
	printf("%.2f\n", sum(7, 7));
}
