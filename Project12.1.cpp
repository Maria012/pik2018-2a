
#include "pch.h"
#include <iostream>
#include<stdio.h>
#include<math.h>

	bool isPrime(int n) {
		for (int i = 2; i <= sqrt(n); i++)
		{
			if (n%i == 0)
			{
				return false;
			}
		}
		return true;
	}
	void listPrimes(int a, int b) {
		if (a > b)
		{
			int swap;
			swap = b;
			b=a ;
			a = swap;
		}
		for (int i = a; i < b; i++)
		{
			if (isPrime(i) && i != 1)
			{
				printf("%d;", i);
			}
		}
	}
	int main() {
		int a, b;
		scanf_s("%d %d", &a, &b);
		printf("Primzahlen:");
		listPrimes(a, b);
		return 0;
	}
    